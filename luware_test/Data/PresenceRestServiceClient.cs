﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using luware_test.BussinessLogic;
using luware_test.Data.Models;

namespace luware_test.Data
{
    /// <summary>
    /// Class that interacts with REST web service which provides information about the presence of the SIP contacts.
    /// </summary>
    public class PresenceRestServiceClient
    {
        /// <summary>
        /// Get presence state of the user with specified SIP URI from the REST web-service
        /// </summary>
        /// <param name="sipUri">SIP Uri of the user</param>
        /// <returns>Presence state.</returns>
        public static async Task<PresenceStates> GetPresenceState(string sipUri)
        {
            var urlString = BuildPresenceServiceUrl(sipUri);

            Uri restRequestUrl;
            if (Uri.TryCreate(urlString, UriKind.Absolute, out restRequestUrl))
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(restRequestUrl);
                request.Accept = "application/xml";

                using (WebResponse response = await request.GetResponseAsync())
                {
                    var serializer = new XmlSerializer(typeof(PresenceResponse));
                    var result = ((PresenceResponse)serializer.Deserialize(response.GetResponseStream()))?.PresenceResult;
                    if (result != null)
                    {
                        return PresenceStateService.ConvertFromInt(result.PresenceState);
                    }
                }
            }
            return PresenceStates.NotFound;
        }

        //TODO string should be moved to config or method BuildPresenceServiceUrl should be completely rewrited with some url builder
        private const string RESTServiceAddress =
            "http://starmind.luware.net:9310/WIS_HttpBinding/GetPresence?contactName={0}";
        private static string BuildPresenceServiceUrl(string sipUri)
        {
            string urlString = string.Format(RESTServiceAddress, sipUri);
            return urlString;
        }
    }
}
