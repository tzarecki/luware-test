﻿namespace luware_test.Data.Models
{
    public enum PresenceStates
    {
        None,
        Available,
        Busy,
        Offline,
        Away,
        BeRightBack,
        DoNotDisturb,
        NotFound
    }
}