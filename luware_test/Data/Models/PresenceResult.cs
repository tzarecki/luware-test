﻿using System.Xml.Serialization;

namespace luware_test.Data.Models
{
    [XmlRoot(ElementName = "GetEndpointPresenceResponse", Namespace = "http://luware.net/WIS/")]
    public class PresenceResponse
    {
        [XmlElement(ElementName = "GetEndpointPresenceResult")]
        public PresenceResult PresenceResult { get; set; }
    }

    [XmlRoot(ElementName = "GetEndpointPresenceResult", Namespace = "http://luware.net/WIS/")]
    public class PresenceResult
    {
        [XmlElement(ElementName = "activityStatus")]
        public string ActivityStatus { get; set; }

        [XmlElement(ElementName = "displayName")]
        public string DisplayName { get; set; }
      
        [XmlElement(ElementName = "presenceState")]
        public int PresenceState { get; set; }

        [XmlElement(ElementName = "sipUri")]
        public string SipUri { get; set; }       
    }
}