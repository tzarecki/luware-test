﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using luware_test.Data;
using luware_test.Data.Models;

namespace luware_test.BussinessLogic
{
    internal static class PresenceStateService
    {
        /// <summary>
        /// Get presence state of the user with specified SIP URI
        /// </summary>
        /// <param name="sipUri">SIP Uri of the user</param>
        /// <returns>Presence state.</returns>
        public static async Task<PresenceStates> GetPresenceState(string sipUri)
        {
            return await PresenceRestServiceClient.GetPresenceState(sipUri);
        }

        /// <summary>
        /// Converts presence number to PresenceStates enumeration.
        /// </summary>
        public static PresenceStates ConvertFromInt(int number)
        {
            if (number <= 0)
                return PresenceStates.NotFound;

            if (number < 3000)
                return PresenceStates.Offline;

            if (number >= 3000 && number <= 5999)
                return PresenceStates.Available;

            if (number >= 6000 && number <= 8999)
                return PresenceStates.Busy;

            if (number >= 9000 && number <= 11999)
                return PresenceStates.DoNotDisturb;

            if (number >= 12000 && number <= 14999)
                return PresenceStates.BeRightBack;

            if (number >= 15000 && number <= 17999)
                return PresenceStates.Away;

            return PresenceStates.Offline;
        }
    }
}
