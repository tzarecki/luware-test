﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using luware_test.Data.Models;

namespace luware_test.ViewModels
{
    public class PresenceIcon : INotifyPropertyChanged
    {

        private SolidColorBrush _presenceIconColor;
        private PresenceStates _state = PresenceStates.NotFound;

        public SolidColorBrush Brush
        {
            get { return _presenceIconColor; }
            private set
            {
                _presenceIconColor = value;
                OnPropertyChanged(nameof(Brush));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public PresenceStates State
        {
            get { return _state; }
            set
            {
                _state = value;
                Brush = new SolidColorBrush(GetPresenceIconColor(value));
            }
        }

        /// <summary>
        /// Determines the color of the presence 
        /// </summary>
        private static Color GetPresenceIconColor(PresenceStates state)
        {
            switch (state)
            {
                case PresenceStates.None:
                    return Colors.Gray;
                case PresenceStates.Available:
                    return Colors.Green;
                case PresenceStates.Busy:
                    return Colors.Red;
                case PresenceStates.Offline:
                    return Colors.Gray;
                case PresenceStates.Away:
                    return Colors.Yellow;
                case PresenceStates.BeRightBack:
                    return Colors.Yellow;
                case PresenceStates.DoNotDisturb:
                    return Colors.Red;
                default:
                    return Colors.Gray;
            }
        }
    }
}
