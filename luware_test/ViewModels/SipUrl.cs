﻿using System;
using luware_test.Properties;

namespace luware_test.ViewModels
{
    public class SipUri
    {
        private string _uri;
        public string Uri
        {
            get { return _uri; }
            set
            {
                Uri uriResult;

                if (!string.IsNullOrWhiteSpace(value) && System.Uri.TryCreate(value, UriKind.Absolute, out uriResult))
                {
                    _uri = value;
                }
                else
                {
                    throw new Exception(Strings.SipUriInvalid);
                }
            }
        }
    }
}
