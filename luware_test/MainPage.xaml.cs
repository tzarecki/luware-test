﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Net.Browser;
using System.Resources;
using luware_test.BussinessLogic;
using luware_test.Data;
using luware_test.Data.Models;
using luware_test.ViewModels;
using Strings = luware_test.Properties.Strings;

namespace luware_test
{
    public partial class MainPage : UserControl
    {
        //ViewModel for displaying presence indicator
        PresenceIcon PresenceIcon { get; } = new PresenceIcon();
        
        //ViewModel for binding and validating SIP url
        SipUri SipUri { get; } = new SipUri();

        public MainPage()
        {
            InitializeComponent();

            //Enable client HTTP handling. https://msdn.microsoft.com/en-us/library/dd920295(v=vs.95).aspx
            //This is necessary for the Silverlight version of the HttpWebRequest (or WebClient) otherwise it throws NotSupportedException
            WebRequest.RegisterPrefix("http://", WebRequestCreator.ClientHttp);
            WebRequest.RegisterPrefix("https://", WebRequestCreator.ClientHttp);

            presenceIcon.DataContext = PresenceIcon;
            urlTextBox.DataContext = SipUri;
        }

        ///Validates the SipUri, gets the presence state and updates the presence icon.
        private async void GetPresence_Click(object sender, RoutedEventArgs e)
        {
            if (Validation.GetHasError(urlTextBox))
                PresenceIcon.State = PresenceStates.NotFound;
            else
            {
                PresenceIcon.State = await PresenceStateService.GetPresenceState(SipUri.Uri);

                if (PresenceIcon.State == PresenceStates.NotFound)
                {
                    MessageBox.Show(Strings.NotFound, Strings.ErrorBoxTitle, MessageBoxButton.OK);
                }
            }
        }
    }
}

